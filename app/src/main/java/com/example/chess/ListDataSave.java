package com.example.chess;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.content.SharedPreferences;

public class ListDataSave {
	private SharedPreferences preferences;
	private SharedPreferences.Editor editor;

	public ListDataSave(Context mContext, String preferenceName) {
		preferences = mContext.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	public <T> void setDataList(String tag, List<T> datalist) {
		if (null == datalist || datalist.size() <= 0)
			return;

		Gson gson = new Gson();
		String strJson = gson.toJson(datalist);
		editor.putString(tag, strJson);
		editor.commit();

	}

	public <T> List<T> getDataList(String tag) {
		List<T> datalist=new ArrayList<T>();
		String strJson = preferences.getString(tag, null);
		if (null == strJson) {
			return datalist;
		}
		Gson gson = new Gson();
		datalist = gson.fromJson(strJson, new TypeToken<List<T>>() {
		}.getType());
		return datalist;

	}

	public List<String> getTagList(){
		List<String> keys = new ArrayList<>();
		Map<String, ?> allEntries = preferences.getAll();
		for (String key : allEntries.keySet()) {
			keys.add(key);
		}
		return keys;
	}

	public void removeFile(String key) {
		editor.remove(key).commit();
	}
}