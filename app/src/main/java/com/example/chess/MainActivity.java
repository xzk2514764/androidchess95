package com.example.chess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.lang.reflect.Type;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import board.Board;
import board.GameRecord;
import board.SquareView;
import pieces.Location;
import pieces.Pawn;
import pieces.Piece;

public class MainActivity extends AppCompatActivity {
    private Board board;
    private SquareView[][] board_view = new SquareView[Location.BOARDSIZE][Location.BOARDSIZE];
    private SquareView curSquare;
    private boolean  blackRound;
    //promotion related
    private PopupWindow promotionWindow;
    private SquareView promotionSv;
    private String instruction;
    private boolean canUndo;
    //win related
    private PopupWindow winWindow;
    //record related
    private List<String> instructions;
    private List<GameRecord> gameRcords= new ArrayList<GameRecord>();
    // record play related
    private GameRecord curGameRecord;
    private int replayIndex;
    private boolean SortByDate = true;
    Context mContext;
    ListDataSave dataSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //newGameUtil();
        setContentView(R.layout.main_selection);
        List<String> instruction = new ArrayList<String>();
        mContext = getApplicationContext();
        dataSave = new ListDataSave(mContext, "ChessGame");
        List<String> keys = dataSave.getTagList();
        for(String key : keys){
            String tag = key.substring(13);
            String time = key.substring(0,13);
            GameRecord record = new GameRecord(this, tag ,dataSave.getDataList(key), time);
            gameRcords.add(record);
        }
        System.out.println(System.currentTimeMillis());
    }

    private void newGameUtil(boolean record) {
        if (!record) {
            setContentView(R.layout.activity_main);
        }
        this.board = new Board();
        drawBoard();
        blackRound = false;
        TextView curRound = (TextView) findViewById(R.id.turn);
        curRound.setText("White");
        instructions = new ArrayList<String>();
    }
    private void newGameUtil() {
        newGameUtil(false);
    }

    private void drawBoard() {
        TableLayout boardView= (TableLayout) findViewById(R.id.board);
        boardView.removeAllViews();
        int h = boardView.getLayoutParams().height;
        for(int r = 0; r < Location.BOARDSIZE; ++r) {
            TableRow boardRow = new TableRow(this);
            boardRow.setWeightSum(8);
            LinearLayout.LayoutParams layoutParams = new TableRow.LayoutParams(0, h/8);
            layoutParams.weight = 1;
            layoutParams.gravity= Gravity.CENTER;
            for(int c=0; c < Location.BOARDSIZE; ++c) {
                SquareView square = new SquareView(this, r, c, board.getPiece(r, c), this.board.getSqaureColor(r,c), this);
                board_view[r][c] = square;
                square.setLayoutParams(layoutParams);
                if(this.board.getSqaureColor(r,c)) {
                    square.setBackgroundColor(Color.rgb(196, 102, 255));
                } else {
                    square.setBackgroundColor(Color.rgb(219, 161, 255));
                }
                int img = board.getSquarePiece(r, c);
                if (img > 0) {
                    square.setImageResource(img);
                }

                square.setScaleType(SquareView.ScaleType.CENTER);
                boardRow.addView(square);
            }
            boardView.addView(boardRow);
        }
    }

    public boolean isRound(boolean isBlack) {
        return this.blackRound == isBlack;
    }

    public void clearSquare() {
        if(curSquare!=null) {
            if(curSquare.isBlack()) {
                curSquare.setBackgroundColor(Color.rgb(196, 102, 255));
            } else {
                curSquare.setBackgroundColor(Color.rgb(219, 161, 255));
            }
            for(Location loc :curSquare.piece().getNextAvailableMoves()) {
                int r = loc.getY();
                int c = loc.getX();
                SquareView sv = board_view[r][c];
                if(sv.isBlack()) {
                    sv.setBackgroundColor(Color.rgb(196, 102, 255));
                } else {
                    sv.setBackgroundColor(Color.rgb(219, 161, 255));
                }
            }
        }
    }

    public void setCurSquare(SquareView s) {
        curSquare = s;
        curSquare.setBackgroundColor(Color.rgb(235, 96, 156));
        for(Location loc :curSquare.piece().getNextAvailableMoves()) {
            int r = loc.getY();
            int c = loc.getX();
            board_view[r][c].setBackgroundColor(Color.rgb(247, 146, 190));
        }
    }

    public boolean isInNextAvalableMoves(SquareView sv) {
        Location location = sv.getLocation();
        if(curSquare == null) {
            return false;
        }

        for(Location loc :curSquare.piece().getNextAvailableMoves()) {
            int r = loc.getY();
            int c = loc.getX();
            if(r == location.getY() && c == location.getX()) {
                return true;
            }
        }

        return false;
    }

    private void popup_promotation() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.promotion, null);

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        promotionWindow = new PopupWindow(popupView, width, height, focusable);
        promotionWindow.showAtLocation(findViewById(R.id.board), Gravity.CENTER, 0, 0);
    }

    public void undo(View v) {
        if(canUndo) {
            Location prevLoc = new Location(0, 0);
            Location newLoc = new Location(0, 0);
            this.board.undo(prevLoc, newLoc);
            this.instructions.remove(this.instructions.size() - 1);
            blackRound = !blackRound;
            synchronizeSquareView(prevLoc);
            synchronizeSquareView(newLoc);
            canUndo = false;
            clearSquare();
            curSquare = null;
            drawBoard();
            TextView curRound = (TextView) findViewById(R.id.turn);
            if(blackRound) {
                curRound.setText("Black");
            } else {
                curRound.setText("White");
            }
        }
    }

    public void ai(View v) {
        clearSquare();
        Location start = new Location(0, 0);
        Location dest = new Location(0, 0);;
        this.board.ai(start, dest, blackRound);
        curSquare = board_view[start.getY()][start.getX()];
        move(board_view[dest.getY()][dest.getX()], true);
    }

    private boolean isWin() {
        if(board.check(blackRound)){
            TextView curRound = (TextView) findViewById(R.id.turn);
            if(blackRound) {
                curRound.setText("Black is in Check");
            } else {
                curRound.setText("White is in Check");
            }
            if(board.checkmate(blackRound)){
                return true;
            }
        }
        return board.isWin(blackRound);
    }

    private void synchronizeSquareView(Location loc) {
        board_view[loc.getY()][loc.getX()].setPiece(this.board.getPiece(loc.getY(), loc.getX()));
        int img = board.getSquarePiece(loc.getY(), loc.getX());
        if (img >= 0) {
            board_view[loc.getY()][loc.getX()].setImageResource(img);
        } else {
            board_view[loc.getY()][loc.getX()].setImageResource(android.R.color.transparent);
        }
    }

    public void move(SquareView sv, boolean autoMove) {
        canUndo = true;
        Piece p = curSquare.piece();
        clearSquare();
        curSquare.setImageResource(android.R.color.transparent);
        curSquare.setPiece(null);
        Location curLoc = curSquare.getLocation();
        Location destLoc = sv.getLocation();

        curSquare = null;
        instruction = Board.ALPHABET.substring(curLoc.getX(), curLoc.getX() + 1) +
                (Location.BOARDSIZE- curLoc.getY()) + " " +
                Board.ALPHABET.substring(destLoc.getX(), destLoc.getX() + 1) +
                (Location.BOARDSIZE- destLoc.getY());
        if (p instanceof Pawn &&
                ((sv.getLocation().getY() == 0 && !blackRound) ||
                        (sv.getLocation().getY() == Location.BOARDSIZE - 1 && blackRound))) {
            if(autoMove) {
                instruction += " Q";
                endMove(sv);
            } else {
                popup_promotation();
                this.promotionSv = sv;
            }
        } else {
            endMove(sv);
        }
    }
    public void move(SquareView sv) {
        move(sv, false);
    }

    private void endMove() {
        Location destLoc = promotionSv.getLocation();
        this.board.move(instruction, !blackRound);
        instructions.add(instruction);
        blackRound = !blackRound;
        TextView curRound = (TextView) findViewById(R.id.turn);
        board.ResetEnpassant(!blackRound);
        if(blackRound) {
            curRound.setText("Black");
        } else {
            curRound.setText("White");
        }

        synchronizeSquareView(destLoc);
        promotionSv = null;
        instruction = null;

        drawBoard();

        if (isWin()) {
            showWinWindow();
            setContentView(R.layout.input_record_name);
            canUndo = false;
        }
    }

    private void showWinWindow() {
        showWinWindow(0);
    }
    private void showWinWindow(int drawOption) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.blackwin, null);
        if(blackRound) {
            popupView = inflater.inflate(R.layout.whitewin, null);
        }

        if(drawOption == 1) {
            popupView = inflater.inflate(R.layout.can_draw, null);
        } else if (drawOption == 2) {
            popupView = inflater.inflate(R.layout.draw, null);
        } else if (drawOption == 3) {
            popupView = inflater.inflate(R.layout.can_back, null);
        } else if (drawOption == 4) {
            popupView = inflater.inflate(R.layout.replay_end, null);
        }

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        winWindow = new PopupWindow(popupView, width, height, focusable);
        winWindow.showAtLocation(findViewById(R.id.board), Gravity.CENTER, 0, 0);
    }

    public void winBack(View v) {
        winWindow.dismiss();
        winWindow=null;
        setContentView(R.layout.main_selection);
    }

    public void backToMainSelection(View v) {
        setContentView(R.layout.main_selection);
    }

    public void back(View v) {
        showWinWindow(3);
    }

    public void newGame(View v) {
        newGameUtil();
    }

    public void draw(View v) {
        showWinWindow(1);
    }

    public void accept(View v) {
        winWindow.dismiss();
        showWinWindow(2);
    }

    public void decline(View v) {
        winWindow.dismiss();
    }

    public void resign(View v) {
        showWinWindow();
        setContentView(R.layout.input_record_name);
    }

    private void record_selection(Boolean SortByDate) {
        if(SortByDate) {
            LinearLayout record_storage = (LinearLayout) findViewById(R.id.record_storage);
            List<GameRecord> tempRcords = new ArrayList<GameRecord>();
            for (GameRecord gr : this.gameRcords) {
                tempRcords.add(gr);
            }
            Collections.sort(tempRcords, (p1, p2) -> {
                Comparator comparator = Collator.getInstance(Locale.ENGLISH);
                return comparator.compare(p1.getTime(), p2.getTime());
            });
            for (GameRecord gr : tempRcords) {
                LinearLayout parent = (LinearLayout) gr.getParent();
                if (parent != null) {
                    parent.removeAllViews();
                }
                record_storage.addView(gr);
            }
        }
        else{
            LinearLayout record_storage = (LinearLayout) findViewById(R.id.record_storage);
            List<GameRecord> tempRcords = new ArrayList<GameRecord>();
            for (GameRecord gr : this.gameRcords) {
                tempRcords.add(gr);
            }
            Collections.sort(tempRcords, (p1, p2) -> {
                Comparator comparator = Collator.getInstance(Locale.ENGLISH);
                return comparator.compare(p1.getName(), p2.getName());
            });

            for (GameRecord gr : tempRcords) {
                    LinearLayout parent = (LinearLayout) gr.getParent();
                    if (parent != null) {
                        parent.removeAllViews();
                    }
                    record_storage.addView(gr);
            }
        }
    }

    public void SortByDate(View v) {
        setContentView(R.layout.record_selection);
        record_selection(SortByDate);
    }

    public void SortByTitle(View v) {
        setContentView(R.layout.record_selection);
        record_selection(!SortByDate);
    }

    public void playRecord(View v) {
        setContentView(R.layout.record_selection);
        record_selection(SortByDate);
    }

    public void saveRecord(View v) {
        winWindow.dismiss();
        setContentView(R.layout.input_record_name);
    }

    public void okSaveRecord(View v) {
        TextView recordName = (TextView) findViewById(R.id.record_input);
        for(GameRecord gr : gameRcords){
            if(gr.getName().toLowerCase().equals(recordName.getText().toString().toLowerCase())){
                TextView Text = (TextView) findViewById(R.id.textView5);
                Text.setText("This name has already been used.");
                return;
            }
        }
        long time = System.currentTimeMillis();
        GameRecord record = new GameRecord(this,recordName.getText().toString(),instructions,time+"");
        System.out.println(time + recordName.getText().toString());
        dataSave.setDataList(time + recordName.getText().toString(), instructions);
        gameRcords.add(record);
        setContentView(R.layout.record_selection);
        record_selection(SortByDate);
    }

    public void setCurGameRecord(GameRecord g) {
        curGameRecord = g;
        setContentView(R.layout.record);
        newGameUtil(true);
        replayIndex = 0;
    }

    public void nextStep(View v) {
        if(replayIndex < curGameRecord.getInstructions().size()) {
            String instruction = curGameRecord.getInstructions().get(replayIndex);
            System.out.println("instruction: " + instruction);
            this.board.move(instruction, !blackRound);
            blackRound = !blackRound;
            for (int r = 0; r < Location.BOARDSIZE; ++r) {
                for (int c = 0; c < Location.BOARDSIZE; ++c) {
                    Location loc = new Location(c, r);
                    synchronizeSquareView(loc);
                }
            }
        }
        replayIndex++;
        if(replayIndex > curGameRecord.getInstructions().size()) {
            showWinWindow(4);
            setContentView(R.layout.record_selection);
            record_selection(SortByDate);
        }
    }

    public void delteRecord(View v) {
        this.gameRcords.remove(curGameRecord);
        dataSave.removeFile(curGameRecord.getTime()+curGameRecord.getName());
        setContentView(R.layout.record_selection);
        record_selection(SortByDate);
    }

    public void cancelSaveRecord(View v) {
        setContentView(R.layout.main_selection);
    }

    private void endMove(SquareView sv) {
        promotionSv = sv;
        endMove();
    }

    public void setPromotion(View v) {
        v.setBackgroundColor(Color.rgb(235, 96, 156));
        System.out.println(v.getId());
        if(v.getId() == R.id.promotion_bishop) {
            instruction += " B";
        } else if (v.getId() == R.id.promotion_queen) {
            instruction += " Q";
        } else if (v.getId() == R.id.promotion_knight) {
            instruction += " N";
        } else if (v.getId() == R.id.promotion_rook){
            instruction += " R";
        }
        promotionWindow.dismiss();
        endMove();
    }
}