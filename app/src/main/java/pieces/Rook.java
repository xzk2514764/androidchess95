package pieces;

import com.example.chess.R;

import java.util.List;
import board.Board;

public class Rook extends Piece{
    private boolean hasMoved = false;
    public Rook(Location location, boolean isBlack) {
        super(location, isBlack, isBlack ? R.drawable.br : R.drawable.wr);
    }

    public void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces) {
        super.clearNextAvailableMoves();
        int x = super.getLocation().getX();
        int y = super.getLocation().getY();
        findAvailableMovesByDirection(x, y, 0, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 1, 0, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, 0, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 0, -1, pieces, enemyPieces);

    }

    private void findAvailableMovesByDirection(int x, int y, int xD, int yD, List<Piece> pieces, List<Piece> enemyPieces) {
        x += xD;
        y += yD;
        while(0 <= x && x < Location.BOARDSIZE && 0 <= y && y < Location.BOARDSIZE) {
            Location curLocation = new Location(x,y);
            super.addAttackRange(curLocation);
            if(hasPiece(curLocation, pieces)) {
                break;
            } else {
                super.addNextAvailableMoves(curLocation);
                if(hasPiece(curLocation, enemyPieces)) {
                    break;
                }

            }
            x += xD;
            y += yD;
        }
    }

    public void move(Location location, Board board) {
        super.move(location, board);
        this.hasMoved = super.hasMoved();
    }

    public boolean canCastle() {
        return !(hasMoved);
    }
}
