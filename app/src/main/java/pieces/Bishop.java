package pieces;

import com.example.chess.R;

import java.util.List;

public class Bishop extends Piece{
    public Bishop(Location location, boolean isBlack) {
        super(location, isBlack, isBlack ? R.drawable.bb : R.drawable.wb);
    }

    public void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces) {
        super.clearNextAvailableMoves();
        int x = super.getLocation().getX();
        int y = super.getLocation().getY();
        findAvailableMovesByDirection(x, y, 1, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 1, -1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, -1, pieces, enemyPieces);

    }

    private void findAvailableMovesByDirection(int x, int y, int xD, int yD, List<Piece> pieces, List<Piece> enemyPieces) {
        x += xD;
        y += yD;
        while(0 <= x && x < Location.BOARDSIZE && 0 <= y && y < Location.BOARDSIZE) {
            Location curLocation = new Location(x,y);
            super.addAttackRange(curLocation);
            if(hasPiece(curLocation, pieces)) {
                break;
            } else {
                super.addNextAvailableMoves(curLocation);
                if(hasPiece(curLocation, enemyPieces)) {
                    break;
                }

            }
            x += xD;
            y += yD;
        }
    }
}
